# Configurable Views Filter Block

Extends the default exposed filters block from Views with additional options to
display only some specific filters per block instance.

The module provides visibility control over the reset button and sort options
as well.

In case of grouping Views exposed filters into collapsible fieldsets (details),
the module provides an option to filter them out and display a plain form on
some particular instance.

Typical scenarios of use are:

* display a subset of exposed filters in a region and the rest in another
* move the Views sort options from the exposed filters area to another one
* combine exposed filters along with facets in the same region

## Requirements

Just a Drupal site with some view with exposed filters on block.

## Installation

Install the Configurable Views Filter Block module as you would normally
install a contributed Drupal module. Visit
<https://www.drupal.org/node/1897420> for further information.

## Maintainers

Current maintainers:

* Manuel Adan - <https://www.drupal.org/u/manueladan>
